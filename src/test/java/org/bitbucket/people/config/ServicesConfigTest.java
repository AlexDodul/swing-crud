package org.bitbucket.people.config;

import org.bitbucket.people.exception.SwingCrudException;
import org.bitbucket.people.services.IPeopleService;
import org.junit.Test;

public class ServicesConfigTest {
    @Test(expected = SwingCrudException.class)
    public void personNotNull(){
        IPeopleService instance = ServicesConfig.peopleServiceMock();
        instance.create(null);
    }
}