package org.bitbucket.people.comands;

import org.bitbucket.people.entity.Person;
import org.bitbucket.people.exception.SwingCrudException;
import org.bitbucket.people.models.PeopleTableModel;
import org.bitbucket.people.view.PeopleTablePanel;
import org.bitbucket.people.view.PersonDialogDb;

import java.awt.event.ActionListener;
import java.lang.reflect.Field;

public class CrudCommandsDb {

    private final PeopleTableModel peopleTableModel;

    private final PersonDialogDb personDialogDb;

    private final PeopleTablePanel peopleTablePanel;

    public CrudCommandsDb(PeopleTableModel peopleTableModel, PersonDialogDb personDialogDb, PeopleTablePanel peopleTablePanel) {
        this.peopleTableModel = peopleTableModel;
        this.personDialogDb = personDialogDb;
        this.peopleTablePanel = peopleTablePanel;
    }

    public ActionListener actionCreate() {
        return e -> {
            this.personDialogDb.setVisible(Boolean.TRUE);
            if (this.personDialogDb.isDialog()) {
                Person p = this.personDialogDb.getPerson();
                this.peopleTableModel.create(p);
                this.peopleTableModel.readAll();
                this.peopleTablePanel.getPeopleTable().revalidate();
                this.peopleTablePanel.repaint();
                this.personDialogDb.clear();
            }
        };
    }

    public ActionListener actionRead() {
        return e -> {
            this.peopleTableModel.readAll();
            this.peopleTablePanel.getPeopleTable().revalidate();
            this.peopleTablePanel.repaint();
        };
    }

    public ActionListener actionUpdate() {
        return e -> {
            this.personDialogDb.setVisible(Boolean.TRUE);
            if (this.personDialogDb.isDialog()) {
                Person p = this.personDialogDb.getPerson();
                this.peopleTableModel.update(p);
                this.peopleTablePanel.getPeopleTable().revalidate();
                this.peopleTablePanel.repaint();
                this.personDialogDb.clear();
                this.peopleTableModel.readAll();
            }
        };
    }

    public ActionListener actionDelete() {
        return e -> {
            int row = 0;
            try {
                row = this.peopleTablePanel.getPeopleTable().getSelectedRow();
            } catch (IndexOutOfBoundsException ex) {
                throw new SwingCrudException("No data selected for deletion");
            }
            Person p = new Person();
            Class<? extends Person> clz = p.getClass();
            Field[] fields = clz.getDeclaredFields();
            try {
                for (int i = 0; i < fields.length - 1; i++) {
                    fields[i + 1].setAccessible(Boolean.TRUE);
                    Object tmp = this.peopleTableModel.getValueAt(row, i);
                    if (long.class.equals(fields[i + 1].getType())) {
                        fields[i + 1].setLong(p, Long.parseLong(String.valueOf(tmp)));
                    }
                    if (int.class.equals(fields[i + 1].getType())) {
                        fields[i + 1].setInt(p, Integer.parseInt(String.valueOf(tmp)));
                    }
                    if (String.class.equals(fields[i + 1].getType())) {
                        fields[i + 1].set(p, tmp);
                    }
                }
            } catch (IllegalAccessException ex) {
                System.out.println(ex.getMessage());
            } catch (IndexOutOfBoundsException ex) {
                System.out.println(ex.getMessage());
            }
            this.peopleTableModel.delete(p);
            this.peopleTableModel.readAll();
            this.peopleTablePanel.getPeopleTable().revalidate();
            this.peopleTablePanel.repaint();
        };
    }
}
