package org.bitbucket.people.comands;

import org.bitbucket.people.config.ServicesConfig;
import org.bitbucket.people.models.PeopleTableModel;
import org.bitbucket.people.services.IPeopleService;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ChooseDbCommands {

    private final PeopleTableModel peopleTableModel;

    public ChooseDbCommands(PeopleTableModel peopleTableModel) {
        this.peopleTableModel = peopleTableModel;
    }

    public ActionListener chooseDatabase() {
        return e -> {
            JComboBox<String> cmb = (JComboBox) e.getSource();
            String str = (String) cmb.getSelectedItem();
            IPeopleService instance = ServicesConfig.get(str);
            this.peopleTableModel.setPeopleService(instance);
        };
    }
}
