package org.bitbucket.people.comands;

import org.bitbucket.people.view.PersonDialogDb;

import java.awt.event.ActionListener;

public class PersonDialogCommands {

    private boolean isDialog = false;

    private PersonDialogDb personDialogDb;

    public void setPersonDialogDb(PersonDialogDb personDialogDb) {
        this.personDialogDb = personDialogDb;
    }

    public ActionListener actionOk() {
        return e -> {
            this.isDialog = true;
            this.personDialogDb.setVisible(Boolean.FALSE);
        };
    }

    public ActionListener actionCancel() {
        return e -> {
            this.isDialog = false;
            this.personDialogDb.setVisible(Boolean.FALSE);
        };
    }

    public boolean isDialog() {
        return isDialog;
    }
}
