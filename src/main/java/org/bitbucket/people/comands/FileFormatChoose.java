package org.bitbucket.people.comands;

import org.bitbucket.people.config.FormatsConfig;
import org.bitbucket.people.exception.SwingCrudException;
import org.bitbucket.people.services.IPeopleService;

import java.io.File;

public class FileFormatChoose {

    public static IPeopleService chooseService(File file){

        String extension = "";
        int i = file.getName().lastIndexOf('.');
        if (i > 0) {
            extension = file.getName().substring(i + 1);
        }
        switch (extension){
            case "bin":
                return FormatsConfig.peopleServiceBin(file.getAbsolutePath());
            case "csv":
                return FormatsConfig.peopleServiceCsv(file.getAbsolutePath());
            case "json":
                return FormatsConfig.peopleServiceJson(file.getAbsolutePath());
            case "xml":
                return FormatsConfig.peopleServiceXml(file.getAbsolutePath());
            case "yml":
                return FormatsConfig.peopleServiceYml(file.getAbsolutePath());
        }
        throw new SwingCrudException("File format not supported");
    }
}
