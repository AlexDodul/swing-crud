package org.bitbucket.people.comands;

import org.bitbucket.people.config.FormatsConfig;
import org.bitbucket.people.entity.Person;
import org.bitbucket.people.models.PeopleTableModel;
import org.bitbucket.people.view.PeopleTablePanel;
import org.bitbucket.people.view.PersonDialogFile;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.reflect.Field;

public class CrudCommandsFile {

    private final PeopleTableModel peopleTableModel;

    private final PersonDialogFile personDialogFile;

    private final PeopleTablePanel peopleTablePanel;

    private static final JFileChooser fileChooser = new JFileChooser();

    public CrudCommandsFile(PeopleTableModel peopleTableModel, PersonDialogFile personDialogFile, PeopleTablePanel peopleTablePanel) {
        this.peopleTableModel = peopleTableModel;
        this.personDialogFile = personDialogFile;
        this.peopleTablePanel = peopleTablePanel;
        getFileChooser();
    }

    private void getFileChooser(){
        FileNameExtensionFilter[] fileFilter = new FileNameExtensionFilter[5];
        fileFilter[0] = new FileNameExtensionFilter("Bin file", "bin");
        fileFilter[1] = new FileNameExtensionFilter("Csv file", "csv");
        fileFilter[2] = new FileNameExtensionFilter("Json file", "json");
        fileFilter[3] = new FileNameExtensionFilter("Xml file", "xml");
        fileFilter[4] = new FileNameExtensionFilter("Yml file", "yml", "yaml");
        for (FileNameExtensionFilter filter : fileFilter) {
            fileChooser.addChoosableFileFilter(filter);
        }
    }

    public ActionListener actionCreate() {
        return e -> {
            this.personDialogFile.setVisible(Boolean.TRUE);
            if (this.personDialogFile.isDialog()) {
                Person p = this.personDialogFile.getPerson();
                this.peopleTableModel.create(p);
                this.peopleTablePanel.getPeopleTable().revalidate();
                this.peopleTablePanel.repaint();
                this.personDialogFile.clear();
            }
        };
    }

    public ActionListener actionRead() {
        return e -> {
            fileChooser.setAcceptAllFileFilterUsed(false);
            int result = fileChooser.showOpenDialog(this.peopleTablePanel);
            if (result == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                peopleTablePanel.peopleTableModel().setPeopleService(FileFormatChoose.chooseService(file));
                peopleTablePanel.peopleTableModel().refresh();
                this.peopleTablePanel.getPeopleTable().revalidate();
                peopleTablePanel.repaint();
                }
        };
    }

    public ActionListener actionUpdate() {
        return e -> {
            this.personDialogFile.setVisible(Boolean.TRUE);
            if (this.personDialogFile.isDialog()) {
                Person p = this.personDialogFile.getPerson();
                this.peopleTableModel.update(p);
                this.peopleTablePanel.getPeopleTable().revalidate();
                this.peopleTablePanel.repaint();
                this.personDialogFile.clear();
            }
        };
    }

    public ActionListener actionDelete() {
        return e -> {
            int row = this.peopleTablePanel.getPeopleTable().getSelectedRow();
            Person p = new Person();
            Class<? extends Person> clz = p.getClass();
            Field[] fields = clz.getDeclaredFields();
            try {
                for (int i = 0; i < fields.length - 1; i++) {
                    fields[i + 1].setAccessible(Boolean.TRUE);
                    Object tmp = this.peopleTableModel.getValueAt(row, i);
                    if (long.class.equals(fields[i + 1].getType())) {
                        fields[i + 1].setLong(p, Long.parseLong(String.valueOf(tmp)));
                    }
                    if (int.class.equals(fields[i + 1].getType())) {
                        fields[i + 1].setInt(p, Integer.parseInt(String.valueOf(tmp)));
                    }
                    if (String.class.equals(fields[i + 1].getType())) {
                        fields[i + 1].set(p, tmp);
                    }
                }
            } catch (IllegalAccessException ex) {
                System.out.println(ex.getMessage());
            }
            this.peopleTableModel.delete(p);
            this.peopleTablePanel.getPeopleTable().revalidate();
            this.peopleTablePanel.repaint();
        };
    }
}
