package org.bitbucket.people.comands;

import org.bitbucket.people.view.PersonDialogFile;

import java.awt.event.ActionListener;

public class FileDialogCommands {

    private boolean isDialog = false;

    private PersonDialogFile personDialogFile;

    public void setPersonDialogFile(PersonDialogFile personDialogFile) {
        this.personDialogFile = personDialogFile;
    }

    public ActionListener actionOk() {
        return e -> {
            this.isDialog = true;
            this.personDialogFile.setVisible(Boolean.FALSE);
        };
    }

    public ActionListener actionCancel() {
        return e -> {
            this.isDialog = false;
            this.personDialogFile.setVisible(Boolean.FALSE);
        };
    }

    public boolean isDialog() {
        return isDialog;
    }
}
