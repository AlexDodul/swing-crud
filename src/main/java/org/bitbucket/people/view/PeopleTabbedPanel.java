package org.bitbucket.people.view;

import javax.swing.*;

public class PeopleTabbedPanel extends JTabbedPane {

    public PeopleTabbedPanel(PeopleCrudCommandsDbPanel peopleCrudCommandsPanel, PeopleCrudCommandsFilePanel peopleCrudCommandsFilePanel){
        setBounds(720, 10, 300, 500);
        addTab("Work with databases", peopleCrudCommandsPanel);
        addTab("Working with files", peopleCrudCommandsFilePanel);

        setVisible(Boolean.TRUE);
    }

}
