package org.bitbucket.people.view;

import org.bitbucket.people.comands.ChooseDbCommands;
import org.bitbucket.people.comands.CrudCommandsDb;
import org.bitbucket.people.config.ServicesConfig;

import javax.swing.*;

public class PeopleCrudCommandsDbPanel extends JPanel {

    public PeopleCrudCommandsDbPanel(CrudCommandsDb commands, ChooseDbCommands command) {
        setLayout(null);
        setBounds(720, 10, 300, 200);

        JButton btnCreate = new JButton("Create");
        JButton btnRead = new JButton("Read");
        JButton btnUpdate = new JButton("Update");
        JButton btnDelete = new JButton("Delete");

        btnCreate.setBounds(10, 30, 280, 30);
        btnRead.setBounds(10, 80, 280, 30);
        btnUpdate.setBounds(10, 130, 280, 30);
        btnDelete.setBounds(10, 180, 280, 30);

        btnCreate.addActionListener(commands.actionCreate());
        btnRead.addActionListener(commands.actionRead());
        btnUpdate.addActionListener(commands.actionUpdate());
        btnDelete.addActionListener(commands.actionDelete());

        JLabel jLabel = new JLabel("Choose database");
        jLabel.setBounds(10, 240, 280, 30);
        JComboBox<String> cmbRepo = new JComboBox<>(ServicesConfig.keys());
        cmbRepo.setBounds(10, 270, 280, 30);
        cmbRepo.addActionListener(command.chooseDatabase());

        add(btnCreate);
        add(btnRead);
        add(btnUpdate);
        add(btnDelete);
        add(jLabel);
        add(cmbRepo);

        setVisible(Boolean.TRUE);
    }
}
