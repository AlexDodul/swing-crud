package org.bitbucket.people.view;

import org.bitbucket.people.comands.FileDialogCommands;
import org.bitbucket.people.entity.Person;

import javax.swing.*;

public class PersonDialogFile extends JDialog {

    private FileDialogCommands commands;

    private JTextField txtID;

    private JTextField txtFirstName;

    private JTextField txtLastName;

    private JTextField txtAge;

    private JTextField txtCity;

    public PersonDialogFile(FileDialogCommands commands) {
        setLayout(null);
        setBounds(300,300,330,470);
        setResizable(false);
        setModal(Boolean.TRUE);

        this.txtID = new JTextField();
        this.txtFirstName = new JTextField();
        this.txtLastName = new JTextField();
        this.txtAge = new JTextField();
        this.txtCity = new JTextField();

        JLabel id = new JLabel("Enter id");
        JLabel firstName = new JLabel("Enter first name");
        JLabel lastName = new JLabel("Enter last name");
        JLabel age = new JLabel("Enter age");
        JLabel city = new JLabel("Enter city");

        id.setBounds(20,10,250,30);
        firstName.setBounds(20,80,250,30);
        lastName.setBounds(20,150,250,30);
        age.setBounds(20,220,250,30);
        city.setBounds(20,290,250,30);

        this.txtID.setBounds(20,40,250,30);
        this.txtFirstName.setBounds(20,110,250,30);
        this.txtLastName.setBounds(20,180,250,30);
        this.txtAge.setBounds(20,250,250,30);
        this.txtCity.setBounds(20,320,250,30);

        JButton btnOk = new JButton("OK");
        JButton btnCancel = new JButton("Cancel");

        btnOk.addActionListener(commands.actionOk());
        btnCancel.addActionListener(commands.actionCancel());

        btnOk.setBounds(20,370,120,30);
        btnCancel.setBounds(150,370,120,30);

        add(txtID);
        add(txtFirstName);
        add(txtLastName);
        add(txtAge);
        add(txtCity);
        add(id);
        add(firstName);
        add(lastName);
        add(age);
        add(city);

        add(btnOk);
        add(btnCancel);

        this.commands = commands;
        this.commands.setPersonDialogFile(this);
    }

    public boolean isDialog(){
        return this.commands.isDialog();
    }

    //todo add validation
    public Person getPerson(){
        return new Person(
                Long.parseLong(this.txtID.getText()),
                this.txtFirstName.getText(),
                this.txtLastName.getText(),
                Integer.parseInt(this.txtAge.getText()),
                this.txtCity.getText()
        );
    }

    public void clear(){
        this.txtID.setText("");
        this.txtFirstName.setText("");
        this.txtLastName.setText("");
        this.txtAge.setText("");
        this.txtCity.setText("");
    }
}