package org.bitbucket.people.view;

import org.bitbucket.people.models.PeopleTableModel;

import javax.swing.*;

public class PeopleTablePanel extends JPanel {

    private final JTable peopleTable;

    private final PeopleTableModel peopleTableModel;

    public PeopleTableModel peopleTableModel() {
        return peopleTableModel;
    }

    public PeopleTablePanel(PeopleTableModel peopleTableModel) {
        setLayout(null);
        setBounds(10, 10, 700, 500);
        this.peopleTableModel = peopleTableModel;
        this.peopleTable = new JTable(peopleTableModel);
        JScrollPane scr = new JScrollPane(this.peopleTable);
        scr.setBounds(10, 10, 680, 480);
        add(scr);
        setVisible(Boolean.TRUE);
    }

    public JTable getPeopleTable() {
        return peopleTable;
    }
}
