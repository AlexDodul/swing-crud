package org.bitbucket.people.view;

import javax.swing.*;
import java.awt.*;

public class PeopleForm extends JFrame{

    public PeopleForm(PeopleTablePanel peopleTablePanel,
                      PeopleTabbedPanel tabbedPanel
    ) throws HeadlessException {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(1050, 560);
        add(peopleTablePanel);
        add(tabbedPanel);
        setResizable(false);
        setVisible(Boolean.TRUE);
    }

}
