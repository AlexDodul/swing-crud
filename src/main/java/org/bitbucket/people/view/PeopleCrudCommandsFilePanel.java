package org.bitbucket.people.view;

import org.bitbucket.people.comands.CrudCommandsFile;

import javax.swing.*;

public class PeopleCrudCommandsFilePanel extends JPanel {

    public PeopleCrudCommandsFilePanel(CrudCommandsFile commands){
        setLayout(null);
        setBounds(720, 10, 300, 200);

        JButton btnCreate = new JButton("Create");
        JButton btnRead = new JButton("Read");
        JButton btnUpdate = new JButton("Update");
        JButton btnDelete = new JButton("Delete");

        btnCreate.setBounds(10, 30, 280, 30);
        btnRead.setBounds(10, 80, 280, 30);
        btnUpdate.setBounds(10, 130, 280, 30);
        btnDelete.setBounds(10, 180, 280, 30);

        btnCreate.addActionListener(commands.actionCreate());
        btnRead.addActionListener(commands.actionRead());
        btnUpdate.addActionListener(commands.actionUpdate());
        btnDelete.addActionListener(commands.actionDelete());

        add(btnCreate);
        add(btnRead);
        add(btnUpdate);
        add(btnDelete);

        setVisible(Boolean.TRUE);
    }

}
