package org.bitbucket.people.repository.impl;

import org.apache.log4j.Logger;
import org.bitbucket.people.entity.Person;
import org.bitbucket.people.repository.IPeopleRepo;
import org.bitbucket.people.utils.JDBConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static org.apache.log4j.Logger.getLogger;

public class PeoplePgRepo implements IPeopleRepo {

    private static final Logger log = getLogger(PeoplePgRepo.class);

    private JDBConnectionPool pool;

    public PeoplePgRepo(JDBConnectionPool pool) {
        this.pool = pool;
    }

    @Override
    public Person save(Person p) {
        long id = 0;
        Connection con = this.pool.connection();
        String sql = "insert into people (first_name, last_name, age, city) values(?, ?, ?, ?)";
        try (PreparedStatement stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1, p.getFirstName());
            stmt.setString(2, p.getLastName());
            stmt.setInt(3, p.getAge());
            stmt.setString(4, p.getCity());
            int row = stmt.executeUpdate();
            if (row != 0) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            log.warn("Enter: {}" + e.getMessage());
        } finally {
            this.pool.parking(con);
        }
        return p.id(id);
    }

    @Override
    public List<Person> findAll() {
        List<Person> result = new ArrayList<>();
        Connection con = this.pool.connection();
        String sql = "select * from people";
        try (PreparedStatement stmt = con.prepareStatement(sql)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Person p = new Person(
                        rs.getLong("id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getInt("age"),
                        rs.getString("city")
                );
                result.add(p);
            }
        } catch (SQLException e) {
            log.warn("Enter: {}" + e.getMessage());
        } finally {
            this.pool.parking(con);
        }
        return result;
    }

    @Override
    public void update(Person p) {
        long id = 0;
        Connection con = this.pool.connection();
        String sql = "update people " +
                "set first_name = ?, last_name = ?, age = ?, city = ?" +
                "where id = ?";
        try (PreparedStatement stmt = con.prepareStatement(sql)) {
            stmt.setString(1, p.getFirstName());
            stmt.setString(2, p.getLastName());
            stmt.setInt(3, p.getAge());
            stmt.setString(4, p.getCity());
            stmt.setLong(5, p.getId());
            int row = stmt.executeUpdate();
            if (row != 0) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            log.warn("Enter: {}" + e.getMessage());
        } finally {
            this.pool.parking(con);
        }
        p.id(id);
    }

    @Override
    public void remove(long id) {
        Connection con = this.pool.connection();
        String sql = "delete from people where id = ?";
        try (PreparedStatement stmt = con.prepareStatement(sql)) {
            stmt.setLong(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            log.warn("Enter: {}" + e.getMessage());
        } finally {
            this.pool.parking(con);
        }
    }
}
