package org.bitbucket.people.repository.impl;

import org.bitbucket.people.entity.Person;
import org.bitbucket.people.repository.IPeopleRepo;

import java.util.List;

public class PeopleGraphDBRepo implements IPeopleRepo {
    @Override
    public Person save(Person p) {
        return null;
    }

    @Override
    public List<Person> findAll() {
        return null;
    }

    @Override
    public void update(Person p) {

    }

    @Override
    public void remove(long id) {

    }
}
