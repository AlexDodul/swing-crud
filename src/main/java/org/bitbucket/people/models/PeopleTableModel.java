package org.bitbucket.people.models;

import org.bitbucket.people.entity.Person;
import org.bitbucket.people.exception.SwingCrudException;
import org.bitbucket.people.services.IPeopleService;
import org.bitbucket.people.utils.PersonColumn;
import org.bitbucket.people.utils.ReflectionUtils;

import javax.swing.table.AbstractTableModel;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class PeopleTableModel extends AbstractTableModel {

    private List<Person> people = new ArrayList<>();

    private IPeopleService peopleService;

    private List<Field> tableColumns;

    public PeopleTableModel(IPeopleService peopleService, List<Field> tableColumns) {
        this.peopleService = peopleService;
        this.tableColumns = tableColumns;
    }

    public PeopleTableModel refresh() {
        this.people = this.peopleService.readAll();
        return this;
    }

    public void setPeopleService(IPeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return col > 0;
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        Person p = this.people.get(row);
        Class<? extends Person> clz = p.getClass();
        Field[] fields = clz.getDeclaredFields();
        try {
            Field field = fields[col + 1];
            field.setAccessible(Boolean.TRUE);
            Class<?> type = field.getType();
            if (type.equals(int.class)) {
                field.setInt(p, Integer.parseInt((String) value));
            } else {
                field.set(p, value);
            }
        } catch (IllegalAccessException e) {
            System.out.printf("Enter: %s", e.getMessage());
        }
        update(p);
    }

    @Override
    public int getRowCount() {
        return this.people.size();
    }

    @Override
    public int getColumnCount() {
        return this.tableColumns.size();
    }

    @Override
    public String getColumnName(int col) {
        return this.tableColumns.get(col).getAnnotation(PersonColumn.class).name();
    }

    @Override
    public Object getValueAt(int row, int col) {
        Person p;
        try {
            p = this.people.get(row);
        } catch (IndexOutOfBoundsException ex) {
            throw new SwingCrudException("No data selected for deletion");
        }
        Field field = this.tableColumns.get(col);
        return ReflectionUtils.getValue(field, p);
    }

    public void create(Person p) {
        this.peopleService.create(p);
    }

    public void readAll() {
        this.people = this.peopleService.readAll();
    }

    public void update(Person p) {
        this.peopleService.update(p);
    }

    public void delete(Person p) {
        this.peopleService.delete(p.getId());
    }
}
