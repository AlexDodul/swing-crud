package org.bitbucket.people.services.impl;

import org.bitbucket.people.entity.Person;
import org.bitbucket.people.services.IPeopleService;

import java.util.ArrayList;
import java.util.List;

public class PeopleMockService implements IPeopleService {

    public List<Person> people = new ArrayList<>() {{
        add(new Person(1L, "Alex", "Vasechkin", 30, "Kiev"));
        add(new Person(2L, "Dima", "Koval", 31, "Kiev"));
        add(new Person(3L, "Boba", "Azimov", 30, "Kiev"));
    }};

    @Override
    public Person create(Person person) {
        this.people.add(person);
        return person;
    }

    @Override
    public List<Person> readAll() {
        return this.people;
    }

    @Override
    public void update(Person person) {
        String status = "Данный id не найден. Изменения не были внесены";
        for (Person people : this.people) {
            if (person.getId() == people.getId()) {
                this.people.set((int)people.getId() - 1, person);
                status = "Изменения были внесены для id " + people.getId();
            }
        }
        System.out.println(status);
    }

    @Override
    public void delete(long id) {
        this.people.removeIf(p -> p.getId() == id);
    }
}
