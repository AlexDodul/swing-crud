package org.bitbucket.people.services.impl;

import org.bitbucket.people.entity.Person;
import org.bitbucket.people.formats.IFormats;
import org.bitbucket.people.services.IPeopleService;
import org.bitbucket.people.utils.IsPersonValidation;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PeopleCsvService  implements IPeopleService {

    private final IFormats baseFormat;

    private final String fileName;

    private final List<Person> people = new ArrayList<>();

    public PeopleCsvService(IFormats baseFormat, String fileName) {
        this.baseFormat = baseFormat;
        this.fileName = fileName;
    }


    @Override
    public Person create(Person person) {
        if (person == null) {
            throw new RuntimeException("Person is null!");
        }
        IsPersonValidation.isPerson(person);
        this.people.add(person);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(this.fileName, Boolean.FALSE))) {
            bw.write(this.baseFormat.toFormatList(this.people));
        } catch (IOException e) {
            delete(person.getId());
            System.out.println(e.getMessage());
        }
        return person;
    }

    @Override
    public List<Person> readAll() {
        try (BufferedReader br = new BufferedReader(new FileReader(this.fileName))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
            List<Person> pp = this.baseFormat.fromFormatList(sb.toString());
            this.people.addAll(pp);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return this.people;
    }

    @Override
    public void update(Person person) {
        if (person == null) {
            throw new RuntimeException("Person is null!");
        }
        IsPersonValidation.isPerson(person);

        int index = -1;
        for (Person p : people) {
            if (p.getId() == person.getId()) {
                index = people.indexOf(p);
            }
        }
        try {
            this.people.set(index, person);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("This Id not exist!");
        }

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(this.fileName, Boolean.FALSE))) {
            bw.write(this.baseFormat.toFormatList(this.people));
        } catch (IOException e) {
            delete(person.getId());
            System.out.println(e.getMessage());
        }

    }

    @Override
    public void delete(long personId) {
        this.people.removeIf(p -> p.getId() == personId);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(this.fileName, Boolean.FALSE))) {
            bw.write(this.baseFormat.toFormatList(this.people));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void clear() {
        this.people.clear();
    }

    @Override
    public List<Person> getPeople() {
        return this.people;
    }
}
