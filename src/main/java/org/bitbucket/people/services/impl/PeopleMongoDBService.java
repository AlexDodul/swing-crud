package org.bitbucket.people.services.impl;

import org.bitbucket.people.entity.Person;
import org.bitbucket.people.repository.IPeopleRepo;
import org.bitbucket.people.services.IPeopleService;

import java.util.List;

public class PeopleMongoDBService implements IPeopleService {

    private final IPeopleRepo peopleRepo;

    public PeopleMongoDBService(IPeopleRepo peopleRepo) {
        this.peopleRepo = peopleRepo;
    }

    @Override
    public Person create(Person person) {
        return this.peopleRepo.save(person);
    }

    @Override
    public List<Person> readAll() {
        return this.peopleRepo.findAll();
    }

    @Override
    public void update(Person person) {
        this.peopleRepo.update(person);
    }

    @Override
    public void delete(long id) {
        this.peopleRepo.remove(id);
    }
}
