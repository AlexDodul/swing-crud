package org.bitbucket.people.services.impl;

import org.bitbucket.people.entity.Person;
import org.bitbucket.people.repository.IPeopleRepo;
import org.bitbucket.people.services.IPeopleService;

import java.util.List;

public class PeopleRedisService implements IPeopleService {

    private final IPeopleRepo peopleRepo;

    public PeopleRedisService(IPeopleRepo peopleRepo) {
        this.peopleRepo = peopleRepo;
    }

    @Override
    public Person create(Person person) {
        return null;
    }

    @Override
    public List<Person> readAll() {
        return null;
    }

    @Override
    public void update(Person person) {

    }

    @Override
    public void delete(long id) {

    }
}
