package org.bitbucket.people.services.impl;

import org.bitbucket.people.entity.Person;
import org.bitbucket.people.formats.IFormats;
import org.bitbucket.people.services.IPeopleService;
import org.bitbucket.people.utils.IsPersonValidation;

import java.util.ArrayList;
import java.util.List;

public class PeopleBinService implements IPeopleService {

    private final String path;

    private final IFormats baseFormat;

    private final List<Person> people = new ArrayList<>();


    public PeopleBinService(IFormats baseFormat, String path) {
        this.baseFormat = baseFormat;
        this.path = path;
    }

    @Override
    public Person create(Person person) {
        if (person == null) {
            throw new RuntimeException("Person is null!");
        }
        IsPersonValidation.isPerson(person);

        this.people.add(person);
        this.baseFormat.saveObject(this.people, this.path);
        return person;
    }

    @Override
    public List<Person> readAll() {
        List<Person> all = this.baseFormat.loadObject(this.path);
        this.people.addAll(all);
        return all;
    }

    @Override
    public void update(Person person) {
        if (person == null) {
            throw new RuntimeException("Person is null!");
        }
        IsPersonValidation.isPerson(person);

        int index = -1;
        for (Person p : people) {
            if (p.getId() == person.getId()) {
                index = people.indexOf(p);
            }
        }
        try {
            this.people.set(index, person);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("This Id not exist!");
        }
        this.baseFormat.saveObject(this.people, this.path);
    }

    @Override
    public void delete(long personId) {
        this.people.removeIf(p -> p.getId() == personId);
        this.baseFormat.saveObject(this.people, this.path);
    }

    @Override
    public void clear() {
        this.people.clear();
    }

    @Override
    public List<Person> getPeople() {
        return this.people;
    }
}
