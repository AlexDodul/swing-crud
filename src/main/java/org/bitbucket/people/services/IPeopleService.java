package org.bitbucket.people.services;

import org.bitbucket.people.entity.Person;
import org.bitbucket.people.utils.PersonNotNull;

import java.util.List;

public interface IPeopleService {

    Person create(@PersonNotNull Person person);

    List<Person> readAll();

    void update(@PersonNotNull Person person);

    void delete(long id);

    default void clear(){}

    default List<Person> getPeople() {
        return null;
    }

}
