package org.bitbucket.people.utils;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class JDBConnectionPool {

    private static final Logger log = Logger.getLogger(JDBConnectionPool.class);

    private long timeout;

    private String url, user, pass;

    private AtomicInteger count;

    private Map<Connection, ConnectionEntry> pool = new HashMap<>();

    public JDBConnectionPool(long timeout, String driver, String url, String user, String pass) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        this.timeout = timeout;
        this.url = url;
        this.user = user;
        this.pass = pass;
        this.count = new AtomicInteger(0);
    }

    private Connection doConnect() {
        try {
            return DriverManager.getConnection(this.url, this.user, this.pass);
        } catch (SQLException e) {
            log.warn("Enter: " + e.getMessage());
        }
        return null;
    }

    private void expire(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean validate(Connection connection) {
        try {
            return !connection.isClosed();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public synchronized Connection connection() {
        long now = System.currentTimeMillis();
        for (Connection connection : this.pool.keySet()) {
            ConnectionEntry entry = this.pool.get(connection);
            if ((now - entry.createAt) > this.timeout) {
                expire(connection);
                this.pool.remove(connection);
            } else {
                if (validate(connection)) {
                    if (entry.status == ConnectionStatus.off) {
                        entry.status = ConnectionStatus.on;
                        log.info("Enter " + entry);
                        return connection;
                    }
                } else {
                    expire(connection);
                    this.pool.remove(connection);
                }
            }
        }
        Connection connection = doConnect();
        ConnectionEntry entry = new ConnectionEntry(
                this.count.incrementAndGet(), ConnectionStatus.on, now
        );
        log.info("Enter: " + entry);
        this.pool.put(connection, entry);
        return connection;
    }

    public synchronized void parking(Connection connection) {
        long now = System.currentTimeMillis();
        ConnectionEntry entry = this.pool.get(connection);
        entry.status = ConnectionStatus.off;
        entry.createAt = now;
    }

    private class ConnectionEntry {

        private int number;

        private ConnectionStatus status;

        private long createAt;

        public ConnectionEntry(int number, ConnectionStatus status, long createAt) {
            this.number = number;
            this.status = status;
            this.createAt = createAt;
        }

        @Override
        public String toString() {
            return "Connection number = " + number;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ConnectionEntry entry = (ConnectionEntry) o;
            return number == entry.number &&
                    createAt == entry.createAt &&
                    Objects.equals(status, entry.status);
        }

        @Override
        public int hashCode() {
            return Objects.hash(number, status, createAt);
        }
    }

    private enum  ConnectionStatus {
        on, off
    }
}
