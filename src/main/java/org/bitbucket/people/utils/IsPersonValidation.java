package org.bitbucket.people.utils;

import org.bitbucket.people.entity.Person;

public class IsPersonValidation {

    public static void isPerson(Person data) {
        if (data.getId() <= 0) {
            throw new RuntimeException("Person has a not valid Id! Check and try again.");
        } else if (data.getFirstName() == null || data.getAge() <= 0 ||
                "".equals(data.getFirstName()) || data.getLastName() == null ||
                "".equals(data.getLastName()) || data.getCity() == null || "".equals(data.getCity())) {
            throw new RuntimeException("Person has a not valid parameter! Check and try again.");
        }
    }

}
