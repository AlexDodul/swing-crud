package org.bitbucket.people.handlers;

import org.apache.log4j.Logger;
import org.bitbucket.people.exception.SwingCrudException;
import org.bitbucket.people.services.IPeopleService;
import org.bitbucket.people.utils.PersonNotNull;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Objects;

import static org.apache.log4j.Logger.getLogger;

public class PersonServiceHandler implements InvocationHandler {

    private static final Logger log = getLogger(PersonServiceHandler.class);

    private final IPeopleService peopleService;

    public PersonServiceHandler(IPeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log.info("Before call to method: " + method.getName() + " with args: " + args);
        Parameter[] params = method.getParameters();
        for (int i = 0; i < params.length; i++) {
            if (params[i].isAnnotationPresent(PersonNotNull.class)){
                if (Objects.isNull(args[i])){
                    throw new SwingCrudException("Person can not be null");
                }
            }
        }
        Object result = method.invoke(peopleService, args);
        log.info("After call to method: " + result);
        return result;
    }
}
