package org.bitbucket.people.config;

import org.bitbucket.people.handlers.PersonServiceHandler;
import org.bitbucket.people.services.IPeopleService;
import org.bitbucket.people.services.impl.*;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class ServicesConfig {

    private static final Map<String, IPeopleService> instances = new HashMap<>() {{
        put("Mock", peopleServiceMock());
        put("Pg", peopleServicePg());
        put("MySql", peopleServiceMySql());
        put("H2", peopleServiceH2());
        put("MongoDB", peopleServiceMongoDB());
        put("Redis", peopleServiceRedis());
        put("Cassandra", peopleServiceCassandra());
        put("GraphDB", peopleServiceGraphDB());
    }};

    public static IPeopleService get(String key){
        return instances.get(key);
    }

    public static String[] keys(){
        return instances.keySet().toArray(String[]::new);
    }

    public static IPeopleService peopleServiceMock(){
        IPeopleService origin = new PeopleMockService();
        InvocationHandler handler = new PersonServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(
                origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler
        );
    }

    public static IPeopleService peopleServicePg(){
        IPeopleService origin = new PeoplePgService(RepositoryConfig.peopleRepoPg());
        InvocationHandler handler = new PersonServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(
                origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler
        );
    }

    public static IPeopleService peopleServiceMySql(){
        IPeopleService origin = new PeopleMySqlService(RepositoryConfig.peopleRepoMySql());
        InvocationHandler handler = new PersonServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(
                origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler
        );
    }

    public static IPeopleService peopleServiceH2(){
        IPeopleService origin = new PeopleH2Service(RepositoryConfig.peopleRepoH2());
        InvocationHandler handler = new PersonServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(
                origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler
        );
    }

    public static IPeopleService peopleServiceCassandra(){
        IPeopleService origin = new PeopleCassandraService(RepositoryConfig.peopleRepoCassandra());
        InvocationHandler handler = new PersonServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(
                origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler
        );
    }

    public static IPeopleService peopleServiceGraphDB(){
        IPeopleService origin = new PeopleGraphDBService(RepositoryConfig.peopleRepoGraphDB());
        InvocationHandler handler = new PersonServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(
                origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler
        );
    }

    public static IPeopleService peopleServiceMongoDB(){
        IPeopleService origin = new PeopleMongoDBService(RepositoryConfig.peopleRepoMongoDB());
        InvocationHandler handler = new PersonServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(
                origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler
        );
    }

    public static IPeopleService peopleServiceRedis(){
        IPeopleService origin = new PeopleRedisService(RepositoryConfig.peopleRepoRedis());
        InvocationHandler handler = new PersonServiceHandler(origin);
        return (IPeopleService) Proxy.newProxyInstance(
                origin.getClass().getClassLoader(),
                new Class[] {IPeopleService.class},
                handler
        );
    }
}

