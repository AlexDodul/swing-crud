package org.bitbucket.people.config;

import org.bitbucket.people.comands.*;
import org.bitbucket.people.entity.Person;
import org.bitbucket.people.models.PeopleTableModel;
import org.bitbucket.people.services.impl.PeopleMockService;
import org.bitbucket.people.utils.ReflectionUtils;
import org.bitbucket.people.view.*;

import javax.swing.*;
import java.lang.reflect.Field;
import java.util.List;

public class ViewConfig {

    private static List<Field> personColumn = ReflectionUtils.fields(Person.class);

    private static PeopleTableModel peopleTableModel =
            new PeopleTableModel(new PeopleMockService(), personColumn)
                    .refresh();

    private static PersonDialogCommands personDialogCommands() {
        return new PersonDialogCommands();
    }

    private static FileDialogCommands fileDialogCommands() {
        return new FileDialogCommands();
    }

    private static ChooseDbCommands chooseDbCommands() {
        return new ChooseDbCommands(peopleTableModel);
    }

    private static CrudCommandsDb crudCommandsBd(PersonDialogDb personDialogDb, PeopleTablePanel peopleTablePanel) {
        return new CrudCommandsDb(peopleTableModel, personDialogDb, peopleTablePanel);
    }

    private static CrudCommandsFile crudCommandsFile(PersonDialogFile personDialogFile, PeopleTablePanel peopleTablePanel) {
        return new CrudCommandsFile(peopleTableModel, personDialogFile, peopleTablePanel);
    }

    private static PeopleCrudCommandsDbPanel peopleCrudCommandsBdPanel(CrudCommandsDb crudCommandsBd, ChooseDbCommands chooseDbCommands) {
        return new PeopleCrudCommandsDbPanel(crudCommandsBd, chooseDbCommands);
    }

    private static PeopleCrudCommandsFilePanel peopleCrudCommandsFilePanel(CrudCommandsFile crudCommandsFile) {
        return new PeopleCrudCommandsFilePanel(crudCommandsFile);
    }

    private static PersonDialogDb personDialogDb() {
        return new PersonDialogDb(personDialogCommands());
    }

    private static PersonDialogFile personDialogFile() {
        return new PersonDialogFile(fileDialogCommands());
    }

    private static PeopleTablePanel peopleTablePanel() {
        return new PeopleTablePanel(peopleTableModel);
    }

    private static PeopleTabbedPanel peopleTabbedPanel(PeopleTablePanel peopleTablePanel) {
        ChooseDbCommands chooseDbCommands = chooseDbCommands();
        CrudCommandsDb crudCommandsBd = crudCommandsBd(personDialogDb(), peopleTablePanel);
        CrudCommandsFile crudCommandsFile = crudCommandsFile(personDialogFile(), peopleTablePanel);
        PeopleCrudCommandsDbPanel crudCmdBd = peopleCrudCommandsBdPanel(crudCommandsBd, chooseDbCommands);
        PeopleCrudCommandsFilePanel crudCmdFile = peopleCrudCommandsFilePanel(crudCommandsFile);
        return new PeopleTabbedPanel(crudCmdBd, crudCmdFile);
    }

    public static JFrame peopleFrame() {
        PeopleTablePanel peopleTablePanel = peopleTablePanel();
        PeopleTabbedPanel peopleTabbedPanel = peopleTabbedPanel(peopleTablePanel);
        return new PeopleForm(peopleTablePanel, peopleTabbedPanel);
    }
}
