package org.bitbucket.people.config;

import org.bitbucket.people.formats.IFormats;
import org.bitbucket.people.formats.impl.*;
import org.bitbucket.people.services.IPeopleService;
import org.bitbucket.people.services.impl.*;

public class FormatsConfig {

    public static IFormats binFormat() {
        return new FormatBin();
    }

    public static IPeopleService peopleServiceBin(String path) {
        return new PeopleBinService(binFormat(), path);
    }

    public static IFormats csvFormat() {
        return new FormatCsv();
    }

    public static IPeopleService peopleServiceCsv(String path) {
        return new PeopleCsvService(csvFormat(), path);
    }

    public static IFormats jsonFormat() {
        return new FormatJson();
    }

    public static IPeopleService peopleServiceJson(String path) {
        return new PeopleJsonService(jsonFormat(), path);
    }

    public static IFormats xmlFormat() {
        return new FormatXml();
    }

    public static IPeopleService peopleServiceXml(String path) {
        return new PeopleXmlService(xmlFormat(), path);
    }

    public static IFormats ymlFormat() {
        return new FormatYml();
    }

    public static IPeopleService peopleServiceYml(String path) {
        return new PeopleYmlService(ymlFormat(), path);
    }

}
