package org.bitbucket.people.config;

import org.apache.log4j.Logger;
import org.bitbucket.people.repository.IPeopleRepo;
import org.bitbucket.people.repository.impl.*;
import org.bitbucket.people.utils.JDBConnectionPool;

public class RepositoryConfig {

    public static IPeopleRepo peopleRepoPg(){

        return new PeoplePgRepo(new JDBConnectionPool(
                30000,
                "org.postgresql.Driver",
                "jdbc:postgresql://localhost:54320/mydb",
                "john",
                "pwd0123456789"
        ));
    }

    public static IPeopleRepo peopleRepoMySql() {
        return new PeopleMySqlRepo(new JDBConnectionPool(
                30000,
                "com.mysql.cj.jdbc.Driver",
                "jdbc:mysql://localhost:3306/mysql",
                "root",
                "secret"
        ));
    }

    public static IPeopleRepo peopleRepoCassandra(){
        return new PeopleCassandraRepo();
    }

    public static IPeopleRepo peopleRepoH2(){
        return new PeopleH2Repo(new JDBConnectionPool(
                30000,
                "org.h2.Driver",
                "jdbc:h2:tcp://localhost:1521/people_dev",
                "",
                ""
        ));
    }

    public static IPeopleRepo peopleRepoMongoDB(){
        return new PeopleMongoDBRepo(
                "localhost",
                27017,
                "root",
                "pass12345",
                "people"
        );
    }

    public static IPeopleRepo peopleRepoGraphDB(){
        return new PeopleGraphDBRepo();
    }

    public static IPeopleRepo peopleRepoRedis(){
        return new PeopleRedisRepo();
    }
}
