package org.bitbucket.people.formats;

import org.bitbucket.people.entity.Person;

import java.util.List;

public interface IFormats {
    String toFormat(Person person);

    Person fromFormat(String str);

    String toFormatList(List<Person> people);

    List<Person> fromFormatList(String str);

    default List<Person> loadObject(String fileName) {
        throw new UnsupportedOperationException();
    }

    default void saveObject(List<Person> people, String fileName) {
        throw new UnsupportedOperationException();
    }
}
