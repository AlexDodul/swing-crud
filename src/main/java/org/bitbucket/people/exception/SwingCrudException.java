package org.bitbucket.people.exception;

public class SwingCrudException extends RuntimeException{
    public SwingCrudException(String message) {
        super(message);
    }
}
